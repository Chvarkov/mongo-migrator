import { IProviders } from './migration/interfaces/provider.interfaces';
import { EventDispatcher } from './common/core/event-dispatcher';
import { DiffComparator } from './migration/core/diff-comparator';
import { GeneratorFactory } from './migration/core/generator-factory';
import { ModelMetadataMapper } from './common/core/model-metadata-mapper';
import { BsonSchematicMaker } from './migration/core/bson-schematic-maker';
import { Connection } from './common/core/connection';
import { MigrationManager } from './migration/core/migration-manager';
import { MigrationMetadataMapper } from './migration/core/metadata-mappers/migration-metadata-mapper';

export const providers: IProviders = [
    {
        name: 'CONNECTION_URL',
        value: process.env.DB_CONNECTION_URL || 'mongodb://localhost:27017', // TODO: Refactor it.
    },
    {
        name: 'DATABASE_NAME',
        value: 'tests',
    },
    {
        singleton: true,
        class: Connection,
        inject: [
            'CONNECTION_URL',
            'DATABASE_NAME',
        ],
    },
    {
        class: EventDispatcher,
    },
    {
        singleton: true,
        class: GeneratorFactory,
    },
    {
        singleton: true,
        class: DiffComparator,
    },
    {
        singleton: true,
        class: ModelMetadataMapper,
    },
    {
        singleton: true,
        class: MigrationMetadataMapper,
    },
    {
        singleton: true,
        class: BsonSchematicMaker,
        inject: [
            ModelMetadataMapper
        ],
    },
    {
        singleton: true,
        class: MigrationManager,
        inject: [
            Connection,
            EventDispatcher,
            GeneratorFactory,
            BsonSchematicMaker,
            DiffComparator,
            ModelMetadataMapper,
            MigrationMetadataMapper,
            'MIGRATION_MANAGER_OPTIONS',
        ],
    }
];

import { ModelMetadataMapper } from '../../common/core/model-metadata-mapper';
import { IModelOptions, IPropertyOptions, IRangeOptions } from '../../migration/interfaces/options.interfaces';
import { ServiceContainer } from '../../common/core/service-container';

const modelMetadataMapper = ServiceContainer.getContainer().get(ModelMetadataMapper);

export function Model(options: IModelOptions) {
    return function decorator<T>(constructor: new() => T) {
        const classname = constructor.name;
        modelMetadataMapper.addModel(options, classname);
        modelMetadataMapper.collectMetadata();
    }
}

export function CompoundIndex() {
    return function decorator<T>(constructor: new() => T) {
        const classname = constructor.name;
        modelMetadataMapper.addModel({collection: classname}, classname);
        modelMetadataMapper.collectMetadata();
    }
}

export function Index(options: any) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, options);
    }
}

export function InternalModel() {
    return function decorator<T>(constructor: new() => T) {
        const classname = constructor.name;
        modelMetadataMapper.addModel({collection: classname}, classname);
        modelMetadataMapper.collectMetadata();
    }
}

export function Property(options: IPropertyOptions) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, options);
    }
}

export function Description(description: string) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, {description});
    }
}

export function Required() {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, {required: true});
    }
}

export function Regex(regex: RegExp) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, {$regex: regex});
    }
}

export function Pattern(pattern: string) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, {pattern});
    }
}

export function Range(options: IRangeOptions) {
    return (target: Object, propertyName: string) => {
        modelMetadataMapper.addMetadata(target, propertyName, options);
    }
}

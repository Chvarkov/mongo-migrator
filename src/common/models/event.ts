import { EventsEnum } from '../../migration/enums/events.enum';
import { Partial } from '../../common/models/partial';

export class Event extends Partial<Event> {
    event: EventsEnum;
    version: string;
}


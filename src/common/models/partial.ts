export class Partial<T> {
    constructor(partial?: Partial<T>) {
        if (partial) {
            Object.assign(this, partial);
        }
    }
}

import {
    IClassProvider,
    IFactoryProvider,
    IInstanceProvider,
    IProviders,
    IValueProvider
} from '../../migration/interfaces/provider.interfaces';
import { Provider, Token } from '../../types';
import { IMap } from '../../common/interfaces/map.interface';

export class ServiceContainer {
    private nameMap: IMap<any> = {};
    private classMap: IMap<any> = {};

    private pool: IMap<any> = {};

    private static providers: IProviders = [];
    private static container: ServiceContainer;

    private constructor() {
        this.createMapping();
    }

    static init(providers: IProviders): ServiceContainer {
        this.providers = providers;

        return this.getContainer();
    }

    static getContainer(): ServiceContainer {
        if (!this.container) {
            this.container = new ServiceContainer();
        }

        return this.container;
    }

    static appendProviders(providers: IProviders): ServiceContainer {
        ServiceContainer.providers = ServiceContainer.providers.concat(providers);

        return this.getContainer().createMapping();
    }

    get<T>(token: Token<T>): T {
        const provider = typeof token === 'string'
            ? this.findProviderByName(token)
            : this.findProviderByClass(token);

        return this.getInstanceByProvider(provider);
    }

    private getInstanceByProvider<T>(provider: Provider<T>): T {
        if (this.isValueProvider(provider)) {
            return provider.value;
        }

        if (this.isClassProvider(provider)) {
            const token = provider.name || provider.class.name;

            return this.createInstance(token, provider, (...args) => {
                return new (provider.class.bind.apply(provider.class, [ null, ...args]));
            });
        }

        if (this.isFactoryProvider(provider)) {
            return this.createInstance(provider.name, provider, (...args: any[]) => provider.factory.apply(args));
        }

        throw new Error(`Provider with name '${name}' is invalid.`);
    }

    private createInstance<Provider extends IInstanceProvider<Instance>, Instance>(
        token: string,
        provider: Provider,
        factory: (...args: any[]) => Instance
    ): Instance {
        if (provider.singleton && this.pool[token]) {
            return this.pool[token];
        }

        const dependencies = provider.inject || [];
        const parameters: any[] = dependencies.map((token: Token<Instance>) => this.get(token));

        const instance = factory(...parameters);

        if (provider.singleton) {
            this.pool[token] = instance;
        }

        return instance;
    }

    private findProviderByName(name: string): Provider<any> {
        if (!this.nameMap[name]) {
            throw new Error(`Provider with name \'${name}\' is not declared.`);
        }

        return this.nameMap[name];
    }

    private findProviderByClass<T>(constructor: new() => T): Provider<T> {
        if (!this.classMap[constructor.name]) {
            throw new Error(`Provider for class \'${name}\' is not declared.`);
        }

        return this.classMap[constructor.name];
    }

    private createMapping(): this {
        for (const provider of ServiceContainer.providers){
            this.validateProvider(provider);
            if (provider.name) {
                this.nameMap[provider.name] = provider;
            }
            if (this.isClassProvider(provider)) {
                this.classMap[provider.class.name] = provider;
            }
        }

        return this;
    }

    private validateProvider(provider: Provider<any>) {
        if (this.isClassProvider(provider)) {
            const countDeclaredDependencies = provider.inject ? provider.inject.length : 0;
            if (countDeclaredDependencies !== provider.class.length) {
                throw new Error(`Dependencies count of class '${provider.class.name} not equal to count declared.'`);
            }
        }
    }

    private isValueProvider(provider: any): provider is IValueProvider<any> {
        return provider.value !== undefined
    }

    private isFactoryProvider(provider: any): provider is IFactoryProvider<object> {
        return provider.factory !== undefined
    }

    private isClassProvider(provider: any): provider is IClassProvider<object> {
        return provider.class !== undefined
    }
}

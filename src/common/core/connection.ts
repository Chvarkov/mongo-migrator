import { Db, MongoClient } from 'mongodb';

export class Connection {

    private db: Db;
    private client: MongoClient;

    private connectionConfig = { useUnifiedTopology: true, useNewUrlParser: true };
    private versionCollectionName = 'migration_versions';

    get database(): Db {
        return this.db;
    }

    constructor(private url: string, private databaseName: string) {
    }

    connect(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            MongoClient.connect(this.url, this.connectionConfig, async (err, client: MongoClient) => {
                if (err) {
                    reject(err);
                    return;
                }

                console.log("Connected successfully to server");
                this.client = client;
                this.db = this.client.db(this.databaseName);
                resolve();
            });
        });
    }

    getAllVersions(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            this.db.collection(this.versionCollectionName).find({}).toArray((err, result) => {
                err ? reject(err) : resolve(result.map(value => value.version));
            });
        });
    }

    insertVersion(version: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            await this.db.collection(this.versionCollectionName).insertOne({version}, (err) => {
                err ? reject(err) : resolve();
            });
        });
    }

    removeVersion(version: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            await this.db.collection(this.versionCollectionName).deleteOne({version}, (err) => {
                err ? reject(err) : resolve();
            });
        });
    }

    createVersionCollection(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.db.createCollection('migration_versions', (err) => {
                err ? reject(err) : resolve();
            });
        });
    }

    getCollectionNames(): Promise<string[]> {
        // this.db.collection('system.namespaces').find().toArray(function(err, items) {});
        return new Promise<string[]>((resolve, reject) => {
            this.db.collection('system.namespaces').find().toArray( (err, collections) => {
                err ? reject(err) : resolve(collections);
            });
        });
    }

    async close(): Promise<void> {
        await this.client.close();
    }
}

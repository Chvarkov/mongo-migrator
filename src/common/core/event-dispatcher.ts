import { EventsEnum } from '../../migration/enums/events.enum';
import { Event } from '../../common/models/event';

export class EventDispatcher {
    private handlers: {[key: string]: Array<(event: Event) => void>} = {};

    addEventListener(event: EventsEnum, handler: (event: Event) => void): this {
        if (!this.handlers[event]) {
            this.handlers[event] = [];
        }
        this.handlers[event].push(handler);

        return this;
    }

    dispatch(event: EventsEnum, object: Event): void {
        if (this.handlers[event]) {
            this.handlers[event].forEach(handler => handler(object));
        }
    }
}

import { ICollectionMap, ICollectionMetadata, ICollectorMap } from '../../migration/interfaces/mapping.interfaces';
import { IModelOptions } from '../../migration/interfaces/options.interfaces';
import { IMap } from '../../common/interfaces/map.interface';

export class ModelMetadataMapper {
    // classname -> collection
    private modelNamingMap: IMap<string> = {};

    // collection -> classname
    private collectionNamingMap: IMap<string> = {};

    // classname => class property => metadata
    private collectionMap: ICollectionMap = {};

    // classname => callbacks
    private collectorMap: ICollectorMap = {};

    // class property => mongo property
    private propertyMap: IMap<string> = {};

    get definedCollections(): string[] {
        return Object.keys(this.modelNamingMap)
            .filter(key => key !== this.modelNamingMap[key])
            .map(key => this.modelNamingMap[key]);
    }

    addProperty(classProperty: string, mongoProperty: string): void {
        this.propertyMap[classProperty] = mongoProperty;
    }

    getProperty(classProperty: string): string {
        if (!this.propertyMap[classProperty]) {
            throw new Error(`Mapping error. Property '${classProperty}' does not exists.`);
        }

        return this.propertyMap[classProperty];
    }

    addModel(options: IModelOptions, classname: string): void {
        this.modelNamingMap[classname] = options.collection;
        this.collectionNamingMap[options.collection] = classname;

        if (!this.collectionMap[classname]) {
            this.collectionMap[classname] = {options, properties: {}};

            return;
        }

        this.collectionMap[classname].options = options;
    }

    getModel(collection: string): string {
        if (!this.collectionNamingMap[collection]) {
            throw new Error(`Mapping error. Model for collection '${collection}' does not exists.`);
        }

        return this.collectionNamingMap[collection];
    }

    // TODO: Unused method
    // getCollection(classname: string): string {
    //     if (!this.modelNamingMap[classname]) {
    //         throw new Error(`Mapping error. Collection for model '${classname}' does not exists.`);
    //     }
    //
    //     return this.modelNamingMap[classname];
    // }

    getCollectionMetadata(collection: string): ICollectionMetadata {
        const classname = this.getModel(collection);
        return this.getCollectionMetadataByClassname(classname);
    }

    getCollectionMetadataByClassname(classname: string): ICollectionMetadata {
        if (!this.collectionMap[classname]) {
            throw new Error(`Mapping error. Metadata for class '${classname}' does not exists.`);
        }

        return this.collectionMap[classname];
    }

    addMetadata(object: Object, classProperty: string, metadata: Object): void {
        const classname = object.constructor.name;
        const callback = () => {
            const property = this.getProperty(classProperty);
            const classname = object.constructor.name;

            if (!this.collectionMap[classname]) {
                throw new Error(`Mapping error. Model \'${classname} does not exists. Try add decorator @Model.\'`)
            }

            this.collectionMap[classname].properties[property] = {
                ...this.collectionMap[classname].properties[property],
                ...metadata
            };
        };

        this.addCallback(classname, classProperty, metadata, callback);
    }

    collectMetadata(): void {
        Object.values(this.collectorMap).forEach(item => {
            Object.values(item).forEach(value => {
                value.primary && value.primary();
                value.decoratorCallbacks.forEach(callback => callback());
            })
        });

        this.collectorMap = {};
    }

    private addCallback(classname: string, classProperty: string, metadata: {[key: string]: any}, callback: () => void): void {
        if (!this.collectorMap[classname]) {
            this.collectorMap[classname] = {};
        }
        if (!this.collectorMap[classname][classProperty]) {
            this.collectorMap[classname][classProperty] = {primary: null, decoratorCallbacks: []};
        }
        if (metadata.name && metadata.type) {
            this.addProperty(classProperty, metadata.name);
            this.collectorMap[classname][classProperty].primary = callback;

            return;
        }

        this.collectorMap[classname][classProperty].decoratorCallbacks.push(callback);
    }
}

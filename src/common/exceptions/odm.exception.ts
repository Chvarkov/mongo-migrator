const GLOBAL_PREFIX = 'FL';
const GLOBAL_CODE = 1;

export class OdmException extends Error {
    code: string = `${GLOBAL_PREFIX}${GLOBAL_CODE}`;

    constructor(code: string, message: string) {
        super(message);
        this.code += code;
    }
}

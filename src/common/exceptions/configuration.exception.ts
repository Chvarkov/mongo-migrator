import { OdmException } from '../../common/exceptions/odm.exception';
import { GlobalErrorCodesEnum } from '../../common/enums/global-error-codes.enum';

export class ConfigurationException extends OdmException {
    constructor(code: string, message: string) {
        super(`${GlobalErrorCodesEnum.Configuration}${code}`, message);
    }
}

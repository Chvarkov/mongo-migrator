import { OdmException } from '../../common/exceptions/odm.exception';
import { GlobalErrorCodesEnum } from '../../common/enums/global-error-codes.enum';

export class DatabaseException extends OdmException {
    constructor(code: string,  message: string) {
        super(`${GlobalErrorCodesEnum.Database}${code}`, message);
    }
}

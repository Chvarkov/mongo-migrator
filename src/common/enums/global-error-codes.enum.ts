export enum GlobalErrorCodesEnum {
    Database = 1,           // Mongo db or driver exceptions
    Configuration = 2,      // Configuration or mapping exceptions
    Runtime = 3,            // Runtime exception when execute operations
}

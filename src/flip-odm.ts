import { ServiceContainer } from './common/core/service-container';
import { providers } from './providers';
import { IManagerOptions } from './migration/core/migration-manager';

ServiceContainer.init(providers);

export class FlipOdm {
    static register(options: IManagerOptions): void {
        const optionsProvider = {
            name: 'MIGRATION_MANAGER_OPTIONS',
            value: options,
        };

        ServiceContainer.appendProviders([optionsProvider]);
    }
}

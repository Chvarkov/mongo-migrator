import { IClassProvider, IFactoryProvider, IValueProvider } from './migration/interfaces/provider.interfaces';

// Model types

export type EnumType = { [key: number]: string } | { [key: string]: string } | string[];
export type BsonType = 'boolean' | 'int' | 'double' | 'string' | Function | EnumType;

// Collection options

export type ValidationLevel = 'off' | 'strict' | 'moderate';
export type ValidationAction = 'error' | 'warn';

// DI

export type Token<T> = (new(...args: any[]) => T) | string;
export type Provider<T> = IValueProvider<T> | IClassProvider<T> | IFactoryProvider<T>;

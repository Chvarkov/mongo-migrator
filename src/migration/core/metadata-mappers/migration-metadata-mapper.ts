import {
    IMigration,
    IMigrationMap,
    IMigrationMetadata,
    IMigrationOptions
} from '../../../migration/interfaces/migrations.interfaces';

export class MigrationMetadataMapper {
    private migrationMap: IMigrationMap = {};

    get definedVersions(): string[] {
        return Object.keys(this.migrationMap);
    }

    getByVersion(version: string): IMigrationMetadata | null {
        return this.migrationMap[version] || null;
    }

    addMigrationMetadata(migration: IMigration, options: IMigrationOptions): this {
        const version = options.version.toString();

        if (this.migrationMap[version]) {
            throw new Error('Duplicate version');
        }

        this.migrationMap[version] = {migration, options: {
            ...options,
            onlyForEnvironments: options.onlyForEnvironments || [],
            excludeEnvironments: options.excludeEnvironments || [],
        }};

        return this;
    }
}

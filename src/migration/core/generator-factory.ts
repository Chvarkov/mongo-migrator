import { IGenerator } from '../../migration/core/generators/abstract-generator';
import { LanguagesEnum } from '../../common/enums/languages-enum';
import { JavascriptGenerator } from '../../migration/core/generators/javascript-generator';
import { TypescriptGenerator } from '../../migration/core/generators/typescript-generator';

export class GeneratorFactory {
    create(language: LanguagesEnum, path: string): IGenerator {
        switch (language) {
            case LanguagesEnum.Javascript: return new JavascriptGenerator(path);
            case LanguagesEnum.Typescript: return new TypescriptGenerator(path);
            default: throw new Error('Not supported language.');
        }
    }
}

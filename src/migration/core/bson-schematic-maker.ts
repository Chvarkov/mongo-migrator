import { ModelMetadataMapper } from '../../common/core/model-metadata-mapper';
import { ICollectionMetadata, IPropertyMap, IPropertyMetadata } from '../../migration/interfaces/mapping.interfaces';
import { IMap } from '../../common/interfaces/map.interface';

export class BsonSchematicMaker {
    private propertiesMapping: IMap<string> = {
        type: 'bsonType',
        description: 'description',
        pattern: 'pattern',
        regex: 'regex',
        min: 'minimum',
        max: 'maximum',
    };

    constructor(private readonly metadataMapper: ModelMetadataMapper) {
    }

    make(collection: string): IMap<any> | null {
        if (!this.metadataMapper.definedCollections.includes(collection)) {
            return null;
        }

        const metadata = this.metadataMapper.getCollectionMetadata(collection);

        const {capped, size, autoIndexId, max, validationLevel, validationAction} = metadata.options;

        const schema: IMap<any> = {};

        if (capped !== undefined) {
            schema.capped = capped;
        }
        if (size !== undefined) {
            schema.size = size;
        }
        if (autoIndexId !== undefined) {
            schema.autoIndexId = autoIndexId;
        }
        if (max !== undefined) {
            schema.max = max;
        }
        if (validationLevel !== undefined) {
            schema.validationLevel = validationLevel;
        }
        if (validationAction !== undefined) {
            schema.validationAction = validationAction;
        }

        return {
            ...schema,
            storageEngine: {},
            validator: {
                $jsonSchema: this.buildObjectSchema(metadata),
            },
            indexOptionDefaults: {},
            // viewOn: '',
            pipeline: [],
            collation: {
                locale: 'en',
                caseLevel: false,
                caseFirst: 'off',
                strength: 3,
                numericOrdering: false,
                alternate: 'non-ignorable',
                maxVariable: 'punct',
                normalization: false,
                backwards: false,
            },
            // writeConcern: {},
        };
    }

    private buildObjectSchema(metadata: ICollectionMetadata): object {
        return {
            bsonType: 'object',
            required: this.findRequiredProperties(metadata),
            properties: this.buildPropertiesSchema(metadata.properties),
        }
    }

    private findRequiredProperties(metadata: ICollectionMetadata): string[] {
        return Object.values(metadata.properties).filter(prop => !!prop.required).map(prop => prop.name);
    }

    private buildPropertiesSchema(properties: IPropertyMap): IMap<any> {
        const data: IMap<any> = {};

        for (const key in properties) {
            data[key] = this.buildPropertySchema(properties[key]);
        }

        return data;
    }

    private buildPropertySchema(property: IPropertyMetadata): IMap<any> {
        const data: IMap<any> = this.buildPropertyOptionsSchema(property);

        if (property.type instanceof Function) { // Model
            const metadata = this.metadataMapper.getCollectionMetadataByClassname(property.type.name);

            return {
                ...data,
                bsonType: 'object',
                properties: this.buildPropertiesSchema(metadata.properties),
            };
        }

        if (Array.isArray(property.type)) { // Enum
            delete data.bsonType;
            return {
                ...data,
                enum: property.type,
            }
        }

        return data; // Primitive

    }

    private buildPropertyOptionsSchema(property: IPropertyMetadata): IMap<any> {
        const allowedKeys = Object.keys(this.propertiesMapping);

        const data: IMap<any> = {};

        for (const key in property) {
            if (allowedKeys.includes(key)) {
                data[this.propertiesMapping[key]] = property[key];
            }
        }

        return data;
    }
}

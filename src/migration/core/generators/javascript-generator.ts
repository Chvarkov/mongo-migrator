import { AbstractGenerator } from '../../../migration/core/generators/abstract-generator';
import { javascriptTemplate } from '../../../migration/templates/javascript.template';
import { CodeBuilder } from '../../../migration/core/builders/code.builder';
import { MigrationCodeBuilder } from '../../../migration/core/builders/migration-code.builder';
import { CollectionDiff } from '../../../migration/models/collection-diff';
import { IMap } from '../../../common/interfaces/map.interface';

export class JavascriptGenerator extends AbstractGenerator {
    async generateEmpty(): Promise<void> {
        const snapshot = this.createSnapshot();
        const functionCode = new CodeBuilder()
            .addTodo('Implement it.')
            .build();

        const code = new MigrationCodeBuilder(javascriptTemplate.migration)
            .setSnapshot(snapshot)
            .setUpCode(functionCode)
            .setDownCode(functionCode)
            .build();

        await this.createFile(`${this.path}/migration-${snapshot}.js`, code);
    }

    async generateDiff(diffs: IMap<CollectionDiff>): Promise<void> {
        const snapshot = this.createSnapshot();
        const code = this.generateDiffByTemplate(diffs, javascriptTemplate)
            .setSnapshot(snapshot)
            .build();

        await this.createFile(`${this.path}/migration-${snapshot}.js`, code);
    }
}

import * as fs from 'fs';
import { ITemplate } from '../../../migration/interfaces/template.interface';
import { CodeBuilder } from '../../../migration/core/builders/code.builder';
import { MigrationCodeBuilder } from '../../../migration/core/builders/migration-code.builder';
import { CollectionDiff } from '../../../migration/models/collection-diff';
import { ICollectionDiff } from '../../../migration/interfaces/diff.interfaces';
import { IMap } from '../../../common/interfaces/map.interface';


export interface IGenerator {
    generateEmpty(): Promise<void>;

    generateDiff(diffs: IMap<ICollectionDiff>): Promise<void>;
}

export abstract class AbstractGenerator implements IGenerator {

    protected regex = {
      code: {
          collectionName: /\$collectionName/g,
          options: /\$options/g,
      },
      json: {
          property: /(['"])?([a-z0-9A-Z_\$]+)(['"])?:/g,
          endLine: /([\'\"\}\]e0-9])(\n)/g,
          quote: /\"/g,
      },
    };

    public constructor(protected path: string) {}

    abstract generateEmpty(): Promise<void>;

    abstract generateDiff(diffs: IMap<ICollectionDiff>): Promise<void>;

    protected generateDiffByTemplate(diffs: IMap<CollectionDiff>, template: ITemplate): MigrationCodeBuilder {
        const functionUp = new CodeBuilder();
        const functionDown = new CodeBuilder();

        for (const collectionName in diffs) {
            const diff = diffs[collectionName];
            if (diff.isNewCollection()) { // Create collection
                functionUp
                    .addComment(`Create collection '${collectionName}'.`)
                    .addExpression(this.getCreateCollectionCode(template, collectionName, diff.modelSchema));

                functionDown
                    .addComment(`Delete collection '${collectionName}'.`)
                    .addExpression(this.getRemoveCollectionCode(template, collectionName));

                continue;
            }

            if (diff.isRemovedCollection()) { // Remove collection
                functionUp
                    .addComment(`Delete collection '${collectionName}'.`)
                    .addExpression(this.getRemoveCollectionCode(template, collectionName));

                functionDown
                    .addComment(`Create collection '${collectionName}'.`)
                    .addExpression(this.getCreateCollectionCode(template, collectionName, diff.dbSchema));

                continue;
            }

            if (diff.isChangedCollection()) {  // Change collection
                const diffUp = diff.getDifferenceFromSchema(diff.modelSchema);
                functionUp
                    .addComment(`Change '${collectionName}' collection.`)
                    .addExpression(this.getChangeCollectionCode(template, collectionName, diffUp));

                const diffDown = diff.getDifferenceFromSchema(diff.dbSchema);
                functionDown
                    .addComment(`Revert changes '${collectionName}' collection.`)
                    .addExpression(this.getChangeCollectionCode(template, collectionName, diffDown));
            }
        }

        return new MigrationCodeBuilder(template.migration)
            .setUpCode(functionUp.build())
            .setDownCode(functionDown.build());
    }

    protected createSnapshot(): string {
        const date = new Date();
        let segments: any[] = [
            date.getMonth() + 1,
            date.getDay() - 1,
            date.getHours(),
            date.getMinutes(),
            date.getSeconds()
        ];

        segments = segments.map(val => val <= 9 ? `0${val}`: `${val}`);

        return [date.getFullYear(), ...segments].join('');
    }

    protected createFile(filename: string, code: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            fs.writeFile(filename, code, (err) => {
                err ? reject(err) : resolve();
            });
        });
    }

    private getCreateCollectionCode(template: ITemplate, name: string, options: object): string {
        return template.createCollection
            .replace(this.regex.code.collectionName, name)
            .replace(this.regex.code.options, this.objectToString(options));
    }

    private getRemoveCollectionCode(template: ITemplate, name: string): string {
        return template.deleteCollection
            .replace(this.regex.code.collectionName, name);
    }

    private getChangeCollectionCode(template: ITemplate, name: string, options: object): string {
        return template.changeCollection
            .replace(this.regex.code.collectionName, name)
            .replace(this.regex.code.options, this.objectToString({collMod: name, ...options}));
    }

    private objectToString(value: object): string {
        return JSON.stringify(value, null, 2)
            .replace(this.regex.json.property, '$2:')
            .replace(this.regex.json.endLine, '$1,\n')
            .replace(this.regex.json.quote, '\'');
    }
}

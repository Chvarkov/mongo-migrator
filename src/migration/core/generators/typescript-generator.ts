import { AbstractGenerator } from '../../../migration/core/generators/abstract-generator';
import { typescriptTemplate } from '../../../migration/templates/typescript.template';
import { MigrationCodeBuilder } from '../../../migration/core/builders/migration-code.builder';
import { CodeBuilder } from '../../../migration/core/builders/code.builder';
import { CollectionDiff } from '../../../migration/models/collection-diff';
import { IMap } from '../../../common/interfaces/map.interface';

export class TypescriptGenerator extends AbstractGenerator {
    async generateEmpty(): Promise<void> {
        const snapshot = this.createSnapshot();

        const functionCode = new CodeBuilder()
            .addTodo('Implement it.')
            .build();

        const code = new MigrationCodeBuilder(typescriptTemplate.migration)
            .setSnapshot(snapshot)
            .setUpCode(functionCode)
            .setDownCode(functionCode)
            .build();

        await this.createFile(`${this.path}/migration-${snapshot}.ts`, code);
    }

    async generateDiff(diffs: IMap<CollectionDiff>): Promise<void> {
        const snapshot = this.createSnapshot();
        const code = this.generateDiffByTemplate(diffs, typescriptTemplate)
            .setSnapshot(snapshot)
            .build();

        await this.createFile(`${this.path}/migration-${snapshot}.ts`, code);
    }
}

import { Connection } from '../../common/core/connection';
import * as fs from 'fs';
import { EventDispatcher } from '../../common/core/event-dispatcher';
import { EventsEnum } from '../../migration/enums/events.enum';
import { Event } from '../../common/models/event';
import { LanguagesEnum } from '../../common/enums/languages-enum';
import { GeneratorFactory } from '../../migration/core/generator-factory';
import { IGenerator } from '../../migration/core/generators/abstract-generator';
import { BsonSchematicMaker } from '../../migration/core/bson-schematic-maker';
import { DiffComparator } from '../../migration/core/diff-comparator';
import { ModelMetadataMapper } from '../../common/core/model-metadata-mapper';
import { CollectionDiff } from '../../migration/models/collection-diff';
import { ICollectionDiff } from '../../migration/interfaces/diff.interfaces';
import { TypescriptGenerator } from '../../migration/core/generators/typescript-generator';
import { MigrationMetadataMapper } from '../../migration/core/metadata-mappers/migration-metadata-mapper';
import { IMigrationMetadata } from '../../migration/interfaces/migrations.interfaces';
import { IMap } from '../../common/interfaces/map.interface';
import { ImpossibleDownMigrationException } from '../../migration/exceptions/impossible-down-migration.exception';
import { UndefinedMigrationException } from '../../migration/exceptions/undefined-migration.exception';
import { ImpossibleUpMigrationException } from '../../migration/exceptions/impossible-up-migration.exception';

export interface IManagerOptions {
    migrationPath: string;
    models: any[];
    language?: LanguagesEnum;
}

export class MigrationManager {
    private readonly generator: IGenerator;

    constructor(
        private readonly connection: Connection,
        private readonly eventDispatcher: EventDispatcher,
        private readonly generatorFactory: GeneratorFactory,
        private readonly schematicMaker: BsonSchematicMaker,
        private readonly diffComparator: DiffComparator,
        private readonly modelMetadataMapper: ModelMetadataMapper,
        private readonly migrationMetadataMapper: MigrationMetadataMapper,
        private readonly options: IManagerOptions,
    ) {
        const language = this.options.language || LanguagesEnum.Typescript;
        this.generator = this.generatorFactory.create(language, this.options.migrationPath);
    }

    async up(toVersion?: string): Promise<void> {
        await this.execute(async () => {
            for (const version of await this.findVersionsToUp(toVersion)) {
                await this.upVersion(version);
            }
        });
    }

    async down(toVersion?: string): Promise<void> {
        await this.execute(async () => {
            for (const version of await this.findVersionsToDown(toVersion)) {
                await this.downVersion(version);
            }
        });
    }

    async diff(): Promise<void> {
        await this.execute(async () => {
            const dbCollections = await this.connection.database.collections();

            const diffs: IMap<ICollectionDiff> = {};
            for (const collection of dbCollections) {
                if (collection.collectionName === 'migration_versions') {
                    continue;
                }

                const optionsDb = await collection.options();
                const currentOptions = this.schematicMaker.make(collection.collectionName) || {};

                const diff = this.diffComparator.getDiff(optionsDb, currentOptions);

                if (this.diffComparator.isEmptyDiff(diff)) {
                    continue;
                }

                diffs[collection.collectionName] = new CollectionDiff({
                    ...diff,
                    dbSchema: optionsDb,
                    modelSchema: currentOptions,
                });

                // TODO: MongoDB driver not contains this property in interface, but database returns it.
                //  Temp solution: Remove the property when collect data for generate diff.
                const {collation} = diffs[collection.collectionName].dbSchema;

                if (collation && collation.version) {
                    delete diffs[collection.collectionName].dbSchema.collation.version;
                }
            }

            for (const collectionName of this.modelMetadataMapper.definedCollections) {
                if (!diffs[collectionName]) { // Added new collection
                    const currentOptions = this.schematicMaker.make(collectionName);
                    const diff = this.diffComparator.getDiff({}, currentOptions);

                    if (this.diffComparator.isEmptyDiff(diff)) {
                        continue;
                    }

                    diffs[collectionName] = new CollectionDiff({
                        ...this.diffComparator.getDiff({}, currentOptions),
                        dbSchema: {},
                        modelSchema: currentOptions,
                    });
                }
            }
            await this.generator.generateDiff(diffs);
        });
    }

    async createMigration(): Promise<void> {
        await this.generator.generateEmpty();
    }

    addEventListener(event: EventsEnum, handler: (e: Event) => void): this {
        this.eventDispatcher.addEventListener(event, handler);

        return this;
    }

    private async upVersion(version: string) {
        const migrationMetadata = this.migrationMetadataMapper.getByVersion(version);
        if (this.isNeedSkip(migrationMetadata)) {
            this.eventDispatcher.dispatch(EventsEnum.Skiped, new Event({event: EventsEnum.BeforeDown, version}));
            return;
        }

        this.eventDispatcher.dispatch(EventsEnum.BeforeUp, new Event({event: EventsEnum.BeforeUp, version}));

        await migrationMetadata.migration.up(this.connection.database);

        await this.connection.insertVersion(version);
        this.eventDispatcher.dispatch(EventsEnum.AfterUp, new Event({event: EventsEnum.AfterUp, version}));
    }

    private async downVersion(version: string) {
        const migrationMetadata = this.migrationMetadataMapper.getByVersion(version);
        if (this.isNeedSkip(migrationMetadata)) {
            this.eventDispatcher.dispatch(EventsEnum.Skiped, new Event({event: EventsEnum.BeforeDown, version}));
            return;
        }

        this.eventDispatcher.dispatch(EventsEnum.BeforeDown, new Event({event: EventsEnum.BeforeDown, version}));

        await migrationMetadata.migration.down(this.connection.database);

        await this.connection.removeVersion(version);
        this.eventDispatcher.dispatch(EventsEnum.AfterDown, new Event({event: EventsEnum.AfterDown, version}));
    }

    private async collectMigrations(): Promise<void> {

        const format = this.generator instanceof TypescriptGenerator ? 'ts' : 'js'; // TODO: Fix hardcode

        fs.readdirSync(this.options.migrationPath).forEach( async file => {
            if (file.split('.').pop() !== format) {
                return;
            }
            await import(`${this.options.migrationPath}/${file}`);
        })
    }

    private async execute(callback: () => Promise<void>) {
        this.eventDispatcher.dispatch(EventsEnum.BeforeMigrate, new Event({event: EventsEnum.BeforeMigrate, version: ''}));
        await this.connection.connect();
        if (!(await this.connection.getCollectionNames() ).includes('migration_versions')) {
            await this.connection.createVersionCollection();
        }
        await this.collectMigrations();
        await callback();
        this.eventDispatcher.dispatch(EventsEnum.AfterMigrate, new Event({event: EventsEnum.AfterMigrate, version: ''}));
        await this.connection.close();
    }

    private async findVersionsToUp(toVersion?: string): Promise<string[]> {
        const definedVersions = this.migrationMetadataMapper.definedVersions;

        if (toVersion && !definedVersions.includes(toVersion)) {
            throw new UndefinedMigrationException(toVersion);
        }

        const currentVersions = await this.connection.getAllVersions();
        const currentMaxVersion = Math.max(...currentVersions.map(value => +value)) || 0;

        if (+toVersion < +currentMaxVersion) {
            throw new ImpossibleDownMigrationException(toVersion);
        }

        let versions = definedVersions.filter(version => +version > +currentMaxVersion);

        if (toVersion) {
            versions = versions.filter(version => +version <= +toVersion);
        }

        return versions.sort((a, b) => +a - +b);
    }

    private async findVersionsToDown(toVersion?: string): Promise<string[]> {
        const definedVersions = this.migrationMetadataMapper.definedVersions;

        if (toVersion && !definedVersions.includes(toVersion)) {
            throw new UndefinedMigrationException(toVersion);
        }

        const currentVersions = await this.connection.getAllVersions();
        const currentMaxVersion = Math.max(...currentVersions.map(value => +value)) || 0;

        if (+toVersion > +currentMaxVersion) {
            throw new ImpossibleUpMigrationException(toVersion);
        }

        let versions = definedVersions.filter(version => +version <= +currentMaxVersion);

        if (toVersion) {
            versions = versions.filter(version => +version >= +toVersion);
        }

        return versions.sort((a, b) => +b - +a);
    }

    private isNeedSkip(metadata: IMigrationMetadata): boolean {
        const env = process.env.NODE_ENV;
        const {onlyForEnvironments, excludeEnvironments} = metadata.options;
        if (!env) {
            return !!onlyForEnvironments.length || !!excludeEnvironments.length;
        }

        if (onlyForEnvironments.length) {
            return !onlyForEnvironments.includes(env);
        }

        return excludeEnvironments.includes(env);
    }
}

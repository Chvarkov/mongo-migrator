import { IDiffInfo } from '../../migration/interfaces/diff.interfaces';
import { IMap } from '../../common/interfaces/map.interface';

export class DiffComparator {

    private diff: IDiffInfo;

    getDiff(oldObject: IMap<any>, newObject: IMap<any>): IDiffInfo {
        this.diff = {
            deleted: [],
            changed: [],
            added: [],
        };

        this.walkByStructureOldObject(oldObject, newObject, []);
        this.walkByStructureNewObject(oldObject, newObject, []);

        return this.diff;
    }

    isEmptyDiff(diff: IDiffInfo): boolean {
        return !diff.added.length && !diff.changed.length && !diff.deleted.length;
    }

    findValueByPath(object: IMap<any>, path: string[]): any {

        if (!path.length) {
            return object;
        }

        const localPath = [...path];
        const currentKey = localPath.shift();

        if (!object[currentKey]) {
            return null;
        }

        return this.findValueByPath(object[currentKey], localPath);
    }

    private walkByStructureOldObject(oldObject: IMap<any>, newObject: IMap<any>, path: string[]): void  {
        for (const key in oldObject) {
            if (typeof oldObject[key] !== 'object' || newObject[key] === undefined) {

                if (newObject[key] === undefined) { // Deleted
                    this.diff.deleted.push([...path, key]);

                    continue;
                }

                if (oldObject[key] !== newObject[key]) { // Changed
                    this.diff.changed.push({value: newObject[key], path: [...path, key]});
                }

                continue;
            }

            // if (typeof newObject[key] !== 'object' || newObject[key] === null) { // TODO: ????
            //     console.warn(newObject[key]);
            //     continue;
            // }

            this.walkByStructureOldObject(oldObject[key], newObject[key], [...path, key]);
        }
    }

    private walkByStructureNewObject(oldObject: IMap<any>, newObject: IMap<any>, path: string[]): void {
        for (const key in newObject) {
            if (oldObject[key] === undefined && newObject[key]) { // Non existed old sub-structure
                this.diff.added.push({value: newObject[key], path: [...path, key]});
                continue;
            }

            if (typeof newObject[key] !== 'object' || newObject[key] === null) {
                continue;
            }

            this.walkByStructureNewObject(oldObject[key], newObject[key], [...path, key]);
        }
    }
}

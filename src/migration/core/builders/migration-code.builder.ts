export class MigrationCodeBuilder {
    private regex = {
        snapshot: /\$snapshot/g,
        up: /\$upCode/g,
        down: /\$downCode/g,
    };

    private code: string;

    constructor(protected initialTemplate: string) {
        this.code = initialTemplate;
    }

    setSnapshot(snapshot: string): this {
        this.code = this.code.replace(this.regex.snapshot, snapshot);

        return this;
    }

    setUpCode(upCode: string): this {
        this.code = this.code.replace(this.regex.up, upCode);

        return this;
    }

    setDownCode(downCode: string): this {
        this.code = this.code.replace(this.regex.down, downCode);

        return this;
    }

    build(): string {
        return this.code;
    }
}

export class CodeBuilder {
    private countTabs = 2;
    private newLineRegex = /\n/g;

    private expressions: string[] = [];

    addExpression(expression: string): this {
        this.expressions.push(expression);

        return this;
    }

    addTodo(todo: string): this {
        this.expressions.push(`// TODO: ${todo}`);

        return this;
    }

    addComment(comment: string): this {
        this.expressions.push(`\n// ${comment}\n`);

        return this;
    }

    setCountTabs(count: number): this {
        this.countTabs = count;

        return this;
    }

    build(): string {
        return this.formatCode(this.expressions.join('\n'));
    }

    private formatCode(code: string): string {
        const tabs = '  '.repeat(this.countTabs);
        return code.replace(this.newLineRegex, `\n${tabs}`);
    }
}

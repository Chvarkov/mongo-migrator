import { ICollectionDiff, IDiffDeleted, IDiffValue } from '../../migration/interfaces/diff.interfaces';
import { Partial } from '../../common/models/partial';
import { IMap } from '../../common/interfaces/map.interface';

export class CollectionDiff extends Partial<CollectionDiff> implements ICollectionDiff {

    deleted: IDiffDeleted[];
    changed: IDiffValue[];
    added: IDiffValue[];
    dbSchema: IMap<any>;
    modelSchema: IMap<any>;

    isNewCollection(): boolean {
        return !!this.added.length && !this.changed.length && !this.deleted.length;
    }

    isRemovedCollection(): boolean {
        return !!this.deleted.length && !this.added.length && !this.changed.length;
    }

    isChangedCollection(): boolean {
        return !!this.changed.length;
    }

    getDifferenceFromSchema(object: IMap<any>): IMap<any> {
        const changes: IMap<any> = {};
        for (const value of this.changed) {
            const rootKey = value.path[0];

            if (changes[rootKey] || !object[rootKey]) {
                continue;
            }

            changes[rootKey] = object[rootKey];
        }

        return changes;
    }
}

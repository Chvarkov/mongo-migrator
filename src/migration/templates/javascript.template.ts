import { ITemplate } from '../../migration/interfaces/template.interface';
import { typescriptTemplate } from '../../migration/templates/typescript.template';

const templateMigrationJavascript = '/**\n' +
    '* Autogenerated flip-odm\n' +
    '*/\n' +
    '\n' +
    'const Migration = require(\'../../../src/migration/decorators/migrations.decorators\');\n' + // TODO: Fix path
    '\n' +
    '@Migration.Migration({version: $snapshot})\n' +
    'export class Migration$snapshot {\n' +
    ' /**\n' +
    '  * @param {Db} db\n' +
    '  * @returns {Promise<void>}\n' +
    '  */\n' +
    '  async up(db) {\n' +
    '    $upCode\n' +
    '  }\n' +
    '\n' +
    ' /**\n' +
    '  * @param {Db} db\n' +
    '  * @returns {Promise<void>}\n' +
    '  */\n' +
    '  async down(db) {\n' +
    '    $downCode\n' +
    '  }\n' +
    '}\n';


export const javascriptTemplate: ITemplate = {
    ...typescriptTemplate,
    migration: templateMigrationJavascript,
};

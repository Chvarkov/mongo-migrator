import { IMigration, IMigrationOptions } from '../../migration/interfaces/migrations.interfaces';
import { ServiceContainer } from '../../common/core/service-container';
import { MigrationMetadataMapper } from '../../migration/core/metadata-mappers/migration-metadata-mapper';

const migrationMetadataMapper = ServiceContainer.getContainer().get(MigrationMetadataMapper);

export function Migration(options: IMigrationOptions) {
    return function decorator<T extends IMigration>(constructor: new() => T) {
        migrationMetadataMapper.addMigrationMetadata(new constructor(), options);
    }
}

export enum EventsEnum {
    BeforeMigrate = 'BeforeMigrate',
    Skiped = 'Skiped',
    BeforeUp = 'BeforeUp',
    AfterUp = 'AfterUp',
    BeforeDown = 'BeforeDown',
    AfterDown = 'AfterDown',
    AfterMigrate = 'AfterMigrate',
}

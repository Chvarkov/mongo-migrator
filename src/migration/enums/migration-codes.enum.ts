export enum MigrationCodesEnum {
    Undefined = '01',
    ImposibleUp = '02',
    ImposibleDown = '03',
}

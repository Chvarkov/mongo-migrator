import { IModelOptions, IPropertyOptions, IRangeOptions } from '../../migration/interfaces/options.interfaces';
import { IMap } from '../../common/interfaces/map.interface';

export interface ICollectorItem {
    primary: () => void;
    decoratorCallbacks: Array<() => void>;
}

export interface ICollectorMap {
    [key: string]: {[key: string]: ICollectorItem};
}

export interface ICollectionMap {
    [key: string]: ICollectionMetadata;
}

export interface ICollectionMetadata {
    options: IModelOptions;
    properties: IPropertyMap;
}

export interface IPropertyMap {
    [key: string]: IPropertyMetadata;
}

export interface IPropertyMetadata extends IPropertyOptions, IRangeOptions, IMap<any> {
    required?: boolean;
    regex?: string;
    pattern?: string;
}

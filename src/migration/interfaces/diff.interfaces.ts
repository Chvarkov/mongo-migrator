import { IMap } from '../../common/interfaces/map.interface';

export interface IDiffValue {
    value: any
    path: string[];
}

export interface IDiffDeleted extends Array<string> {
}

export interface IDiffInfo {
    deleted: IDiffDeleted[];
    changed: IDiffValue[];
    added: IDiffValue[];
}

export interface ICollectionDiff extends IDiffInfo {
    dbSchema: IMap<any>,
    modelSchema: IMap<any>,
}

export interface ITemplate {
    migration: string,
    createCollection: string,
    deleteCollection: string,
    changeCollection: string,
}

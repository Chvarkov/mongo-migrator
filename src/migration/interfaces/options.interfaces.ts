import { BsonType, ValidationAction, ValidationLevel } from '../../types';

export interface IModelOptions {
    collection: string
    capped?: boolean;
    size?: number;
    max?: number;
    autoIndexId?: boolean;
    validationLevel?: ValidationLevel,
    validationAction?: ValidationAction,
}

export interface IRangeOptions {
    min?: number;
    max?: number;
}

export interface IPropertyOptions {
    name: string;
    type: BsonType;
}

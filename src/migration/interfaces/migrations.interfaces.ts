import { Db } from 'mongodb';
import { IMap } from '../../common/interfaces/map.interface';

export interface IMigrationOptions {
    version: number
    onlyForEnvironments?: string[];
    excludeEnvironments?: string[];
}

export interface IMigration {
    up(db: Db): Promise<void>;
    down(db: Db): Promise<void>;
}

export interface IMigrationMetadata {
    migration: IMigration;
    options: IMigrationOptions;
}

export interface IMigrationMap extends IMap<IMigrationMetadata> {
}

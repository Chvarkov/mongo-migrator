import { Provider, Token } from '../../types';

export interface IProviders extends Array<Provider<object>>{
}

export interface INameAwareProvider {
    name?: string;
}

export interface IInstanceProvider<T> extends INameAwareProvider {
    singleton?: boolean;
    inject?: Token<any>[]
}

export interface IValueProvider<T> extends INameAwareProvider {
    value: any;
}

export interface IClassProvider<T> extends IInstanceProvider<T> {
    class: new(...args: any[]) => T;
}

export interface IFactoryProvider<T> extends IInstanceProvider<T> {
    factory: (...args: any[]) => T | Promise<T>;
}

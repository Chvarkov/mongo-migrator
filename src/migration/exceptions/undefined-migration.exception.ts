import { RuntimeException } from '../../common/exceptions/runtime.exception';
import { MigrationCodesEnum } from '../../migration/enums/migration-codes.enum';

export class UndefinedMigrationException extends RuntimeException {
    constructor(version: string) {
        super(MigrationCodesEnum.Undefined, `Migration '${version}' is undefined.`);
    }
}

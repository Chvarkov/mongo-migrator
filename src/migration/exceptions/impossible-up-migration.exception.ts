import { RuntimeException } from '../../common/exceptions/runtime.exception';
import { MigrationCodesEnum } from '../../migration/enums/migration-codes.enum';

export class ImpossibleUpMigrationException extends RuntimeException {
    constructor(version: string) {
        super(
            MigrationCodesEnum.ImposibleUp,
            `Impossible execute up migration, because current version large than '${version}'.`,
        );
    }
}

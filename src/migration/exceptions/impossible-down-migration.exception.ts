import { RuntimeException } from '../../common/exceptions/runtime.exception';
import { MigrationCodesEnum } from '../../migration/enums/migration-codes.enum';

export class ImpossibleDownMigrationException extends RuntimeException {
    constructor(version: string) {
        super(
            MigrationCodesEnum.ImposibleDown,
            `Impossible execute down migration, because current version less than '${version}'.`,
        );
    }
}

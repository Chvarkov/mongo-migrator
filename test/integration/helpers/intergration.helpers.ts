import * as fs from 'fs';

export function removeFile(path: string, filename: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        fs.unlink(`${path}/${filename}`, error => error ? reject(error) : resolve());
    })
}

export async function cleanMigrationFiles(path: string, excludeFiles: string[]): Promise<void> {
    const files = fs.readdirSync(path);

    for (const filename of files) {
        if (!excludeFiles.includes(filename)) {
            await removeFile(path, filename);
        }
    }
}


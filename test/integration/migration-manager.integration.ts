import { FlipOdm } from '../../src/flip-odm';
import { LanguagesEnum } from '../../src/common/enums/languages-enum';
import { ServiceContainer } from '../../src/common/core/service-container';
import { MigrationManager } from '../../src/migration/core/migration-manager';
import { Contact } from './models/contact';
import { Student, Address } from './models/student';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { Connection } from '../../src/common/core/connection';
import { ImpossibleDownMigrationException } from '../../src/migration/exceptions/impossible-down-migration.exception';
import { ImpossibleUpMigrationException } from '../../src/migration/exceptions/impossible-up-migration.exception';
import { UndefinedMigrationException } from '../../src/migration/exceptions/undefined-migration.exception';

jest.setTimeout(30000000); // TODO: Optimization

describe('Migration manager integration.', () => {

    const migrationPath = __dirname + '/migrations';

    const firstVersion = '20191003143006';
    const secondVersion = '20191003223547';

    let manager: MigrationManager;
    let connection: Connection;

    beforeAll(async () => {
        await FlipOdm.register({
            models: [
                Contact,
                Student,
                Address,
            ],
            migrationPath,
            language: LanguagesEnum.Typescript,
        });

        const container = ServiceContainer.getContainer();
        manager = container.get(MigrationManager);
        connection = container.get(Connection);
    });

    afterAll(() => {
        connection.database.dropDatabase();
    });

    test('Negative: Imposible up migration.', async () => {
        await manager.up(firstVersion);
        await asyncExpectThrow(async () => manager.down(secondVersion), ImpossibleUpMigrationException);
    });

    test('Negative: Imposible down migration.', async () => {
        await manager.up(secondVersion);
        await asyncExpectThrow(async () => manager.up(firstVersion), ImpossibleDownMigrationException);
    });

    test('Negative: Undefined version when up.', async () => {
        await asyncExpectThrow(async () => manager.up('10000000000000'), UndefinedMigrationException);
    });

    test('Negative: Undefined version when down.', async () => {
        await asyncExpectThrow(async () => manager.up('30000000000000'), UndefinedMigrationException);
    });
});

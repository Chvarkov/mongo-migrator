import { Description, Model, Property, Required } from '../../../src/common/decorators/model.decorators';
import { ObjectId } from 'bson';
import { InternalModel } from '../../../src/common/decorators/model.decorators';
import { Partial } from '../../../src/common/models/partial';
import { Regex } from '../../../src/common/decorators/model.decorators';
import { Pattern } from '../../../src/common/decorators/model.decorators';
import { Range } from '../../../src/common/decorators/model.decorators';

@InternalModel()
export class Address extends Partial<Address> {
    @Required()
    @Regex(/^[A-Z][a-z]{1,20}$/g)
    @Property({name: 'city', type: 'string'})
    @Description('City name')
    city: string;

    @Required()
    @Pattern('/^[A-Z][a-z]{1,20}$/')
    @Property({name: 'street', type: 'string'})
    @Description('Street name')
    street: string;
}

@Model({
    collection: 'students',
    capped: true,
    size: 2048,
    max: 5,
    autoIndexId: true,
    validationLevel: 'strict',
    validationAction: 'error',
})
export class Student extends Partial<Student> {

    __id: ObjectId;

    @Required()
    @Property({name: 'first', type: 'string'})
    @Description('First name')
    first: string;

    @Required()
    @Property({name: 'last', type: 'string'})
    @Description('Last name')
    last: string;

    @Required()
    @Range({min: 1, max: 5})
    @Property({name: 'course', type: 'int'})
    @Description('Course')
    course: number;

    @Required()
    @Property({name: 'address', type: Address})
    @Description('Address object')
    address: Address;
}

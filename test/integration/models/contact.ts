import { Description, Model, Property, Required } from '../../../src/common/decorators/model.decorators';
import { ObjectId } from 'bson';

@Model({collection: 'contacts'})
export class Contact {

    __id: ObjectId;

    @Required()
    @Property({name: 'first', type: 'string'})
    @Description('First name')
    first: string;

    @Required()
    @Property({name: 'phone', type: 'string'})
    @Description('Phone number')
    phone: string;

    @Required()
    @Property({name: 'email', type: 'string'})
    @Description('Email')
    email: string;
}

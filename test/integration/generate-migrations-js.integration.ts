import { FlipOdm } from '../../src/flip-odm';
import { LanguagesEnum } from '../../src/common/enums/languages-enum';
import { ServiceContainer } from '../../src/common/core/service-container';
import { MigrationManager } from '../../src/migration/core/migration-manager';
import { Contact } from './models/contact';
import { Address, Student } from './models/student';
import { EventsEnum } from '../../src/migration/enums/events.enum';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { Connection } from '../../src/common/core/connection';
import { cleanMigrationFiles } from './helpers/intergration.helpers';

jest.setTimeout(30000); // TODO: Optimization

describe('Generate migration javascript.', () => {

    const migrationPath = __dirname + '/migrations';

    const firstVersion = '20191003143006';
    const secondVersion = '20191003223547';

    const versions = [
        firstVersion,
        secondVersion,
        '20191004181710',
        '20191004181813'
    ];

    const requiredFiles = [
        '.gitkeep',
        ...versions.map(version => `migration-${version}.ts`),
        ...versions.map(version => `migration-${version}.js`),
    ];

    let container: ServiceContainer;
    let manager: MigrationManager;
    let connection: Connection;

    beforeAll(async () => {
        await prepare(LanguagesEnum.Javascript);
    });

    afterAll(async () => {
        await cleanMigrationFiles(migrationPath, requiredFiles);
        await cleanCollections();
        await connection.close();
    });

    test('Positive: Javascript migrations.', async () => {
        await testMigrations(LanguagesEnum.Javascript)
    });

    async function testMigrations(language: LanguagesEnum): Promise<void> {
        await prepare(language);

        manager.addEventListener(EventsEnum.AfterUp, (e) => console.log(`Migration up ${e.version}.`));

        await manager.createMigration();
        await manager.up(firstVersion);
        await asyncExpectThrow(async () => manager.down(secondVersion), Error);

        await manager.down(firstVersion);

        await manager.up(secondVersion);
        await asyncExpectThrow(async () => manager.up(firstVersion), Error);

        await manager.diff();
        await cleanMigrationFiles(migrationPath, requiredFiles);
        await manager.diff();
        await manager.up();
    }

    // Helpers

    async function prepare(language: LanguagesEnum): Promise<void> {
        if (connection) {
            await connection.close();
        }

        await FlipOdm.register({
            models: [
                Contact,
                Student,
                Address,
            ],
            migrationPath,
            language,
        });

        container = ServiceContainer.getContainer();

        connection = container.get(Connection);
        await connection.connect();

        await cleanMigrationFiles(migrationPath, requiredFiles);
        await cleanCollections();

        manager = container.get(MigrationManager);
    }

    async function cleanCollections(): Promise<void> {
        await connection.database.dropDatabase();
    }
});

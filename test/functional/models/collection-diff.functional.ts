import { CollectionDiff } from '../../../src/migration/models/collection-diff';

describe('Collection diff model functional.', () => {

    test('Positive: Get difference from schema.', async () => {
        const object = {
            data: {
                items: [1, 2, 3, 5, 8],
            },
            response: {
                statusCode: 404,
                message: 'Not found',
                stackTrace: [
                    'call 1',
                    'call 2',
                    'call 3',
                ],
            }
        };

        const collectionDiff = new CollectionDiff({
            deleted: [],
            changed: [
                {
                    path: ['data'],
                    value: { value: null },
                },
                {
                    path: ['data', 'value'],
                    value: null,
                }
            ],
            added: [],
            dbSchema: object,
            modelSchema: object,
        });

        const schema = {
            ...object,
            response: {
              ...object.response,
              statusCode: 500,
            },
            path: '/root',
        };

        const diff = collectionDiff.getDifferenceFromSchema(schema);

        expect(Object.keys(diff)).toHaveLength(1);
        expect(diff).toHaveProperty('data');
        expect(diff.data).toEqual(object.data);
    });

    test('Positive: Check is new collection method.', () => {
        const collectionDiff = new CollectionDiff({deleted: [], changed: [], added: [true]});
        expect(collectionDiff.isNewCollection()).toEqual(true);
    });

    test('Positive: Check is removed collection method.', () => {
        const collectionDiff = new CollectionDiff({deleted: [true], changed: [], added: []});
        expect(collectionDiff.isRemovedCollection()).toEqual(true);
    });

    test('Positive: Check is changed collection method.', () => {
        let collectionDiff = new CollectionDiff({deleted: [], changed: [true], added: []});
        expect(collectionDiff.isChangedCollection()).toEqual(true);

        collectionDiff = new CollectionDiff({deleted: [true], changed: [true], added: []});
        expect(collectionDiff.isChangedCollection()).toEqual(true);

        collectionDiff = new CollectionDiff({deleted: [], changed: [true], added: [true]});
        expect(collectionDiff.isChangedCollection()).toEqual(true);

        collectionDiff = new CollectionDiff({deleted: [true], changed: [true], added: [true]});
        expect(collectionDiff.isChangedCollection()).toEqual(true);
    });
});

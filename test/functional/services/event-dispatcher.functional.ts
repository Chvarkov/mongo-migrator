import { EventDispatcher } from '../../../src/common/core/event-dispatcher';
import { EventsEnum } from '../../../src/migration/enums/events.enum';
import { Event } from '../../../src/common/models/event';

describe('Event dispatcher functional.', () => {

    test('Positive: Add event listeners.', () => {
        const eventDispatcher = new EventDispatcher();
        let countExecutes = 0;

        const handler = (event: Event) => {
          expect(event).toBeInstanceOf(Event);
          expect(event.event).toEqual(EventsEnum.AfterMigrate);
          countExecutes++;
        };

        eventDispatcher
            .addEventListener(EventsEnum.AfterMigrate, handler)
            .addEventListener(EventsEnum.AfterMigrate, handler)
            .addEventListener(EventsEnum.BeforeDown, e => e)
            .addEventListener(EventsEnum.BeforeDown, e => e)
            .addEventListener(EventsEnum.AfterUp, (e) => fail(`Event: ${e.event}. Executed non expected handler.`));

        eventDispatcher.dispatch(EventsEnum.BeforeDown, new Event({event: EventsEnum.BeforeDown}));
        eventDispatcher.dispatch(EventsEnum.AfterMigrate, new Event({event: EventsEnum.AfterMigrate}));

        expect(countExecutes).toEqual(2);
    });
});

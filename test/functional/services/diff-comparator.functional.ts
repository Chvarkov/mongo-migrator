import { DiffComparator } from '../../../src/migration/core/diff-comparator';

describe('Diff comparator functional.', () => {

    const diffComparator = new DiffComparator();

    const addedObject = {
        bool: true,
        string: 'string',
        number: 12,
        array: [1, 2, 'etc'],
        object: {
            innerValue: 'val',
        }
    };

    const addedScalar = 5;

    const changedOldObject = {
        bool: false,
        string: 'strong',
        number: 54675,
        array: [1, 2, 3],
        object: {
            innerValue: 'val',
        }
    };

    const changedOldScalar = 'old_value';

    const changedNewObject = {
        bool: true,
        string: 'changed',
        number: 589,
        array: [1, 5, 3],
        object: {
            innerValue: 'test',
        }
    };

    const changedNewScalar = 'new_value';

    const deletedObject = {
        bool: true,
        string: 'string',
        number: 320,
        array: [2, 4, 8, 16],
        object: {
            innerValue: 'val',
        }
    };

    const deletedScalar = 'remove me';


    const oldObject = {
        data: {
          values: {
            changedObject: changedOldObject,
            changedScalar: changedOldScalar,
            deletedObject: deletedObject,
            deletedScalar: deletedScalar,
          },
        },
    };

    const newObject = {
        data: {
            values: {
                changedObject: changedNewObject,
                changedScalar: changedNewScalar,
                addedObject: addedObject,
                addedScalar: addedScalar,
            },
        },
    };

    test('Positive: Get difference.', () => {
        const diff = diffComparator.getDiff(oldObject, newObject);

        expect(diff.added).toHaveLength(2);

        expect(diff.added.shift().value).toEqual(addedObject);
        expect(diff.added.shift().value).toEqual(addedScalar);

        expect(diff.changed).toHaveLength(6);

        const diffBool = diff.changed.shift();
        const actualBool = diffComparator.findValueByPath(newObject, diffBool.path);
        expect(actualBool).toEqual(diffBool.value);

        const diffString = diff.changed.shift();
        const actualString = diffComparator.findValueByPath(newObject, diffString.path);
        expect(actualString).toEqual(diffString.value);

        const diffNumber = diff.changed.shift();
        const actualNumber = diffComparator.findValueByPath(newObject, diffNumber.path);
        expect(actualNumber).toEqual(diffNumber.value);

        const diffArrayItem = diff.changed.shift();
        const actualArrayItem = diffComparator.findValueByPath(newObject, diffArrayItem.path);
        expect(actualArrayItem).toEqual(diffArrayItem.value);

        const diffInnerObject = diff.changed.shift();
        const actualInnerObject = diffComparator.findValueByPath(newObject, diffInnerObject.path);
        expect(actualInnerObject).toEqual(diffInnerObject.value);

        expect(diff.deleted).toHaveLength(2);

        const actualDeletedObject = diffComparator.findValueByPath(oldObject, diff.deleted.shift());
        expect(actualDeletedObject).toEqual(deletedObject);

        const actualDeletedScalar = diffComparator.findValueByPath(oldObject, diff.deleted.shift());
        expect(actualDeletedScalar).toEqual(deletedScalar);
    });

    test('Positive: Empty diff.', () => {
        const diff = diffComparator.getDiff(addedObject, addedObject);

        expect(diff.added).toBeInstanceOf(Array);
        expect(diff.added).toHaveLength(0);
        expect(diff.changed).toBeInstanceOf(Array);
        expect(diff.changed).toHaveLength(0);
        expect(diff.deleted).toBeInstanceOf(Array);
        expect(diff.deleted).toHaveLength(0);

        expect(diffComparator.isEmptyDiff(diff)).toEqual(true);
    });

    test('Negative: Find by non existed path.', async () => {
        const value = diffComparator.findValueByPath({data: {}}, ['data', 'non_exists']);
        expect(value).toEqual(null);
    });
});

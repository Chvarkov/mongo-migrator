import { GeneratorFactory } from '../../../src/migration/core/generator-factory';
import { LanguagesEnum } from '../../../src/common/enums/languages-enum';
import { TypescriptGenerator } from '../../../src/migration/core/generators/typescript-generator';
import { JavascriptGenerator } from '../../../src/migration/core/generators/javascript-generator';

describe('Generator factory functional.', () => {

    const generatorFactory = new GeneratorFactory();

    test('Positive: Create generator.', () => {
        expect(generatorFactory.create(LanguagesEnum.Typescript, 'path')).toBeInstanceOf(TypescriptGenerator);
        expect(generatorFactory.create(LanguagesEnum.Javascript, 'path')).toBeInstanceOf(JavascriptGenerator);
    });

    test('Negative: Create generator.', () => {
        expect(() => generatorFactory.create('invalid' as LanguagesEnum, 'path')).toThrow(Error);
    });
});

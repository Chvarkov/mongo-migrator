import { ServiceContainer } from '../../../src/common/core/service-container';
import { IProviders } from '../../../src/migration/interfaces/provider.interfaces';
import { MigrationCodeBuilder } from '../../../src/migration/core/builders/migration-code.builder';
import { Provider, Token } from '../../../src/types';
import { CodeBuilder } from '../../../src/migration/core/builders/code.builder';

describe('Service container functional.', () => {

    const providers: IProviders = [
        {
            name: 'INVALID_PROVIDER',
        } as Provider<any>,
        {
            name: 'VALUE_PROVIDER',
            value: 'Value',
        },
        {
            class: CodeBuilder,
        },
        {
            name: 'CLASS_PROVIDER',
            class: CodeBuilder,
        },
        {
            name: 'FACTORY_PROVIDER',
            factory: () => new CodeBuilder(),
        },
        {
            name: 'CLASS_PROVIDER_WITH_ARGS',
            class: MigrationCodeBuilder,
            inject: [
                'VALUE_PROVIDER',
            ],
        },
        {
            class: MigrationCodeBuilder,
            inject: [
                'VALUE_PROVIDER',
            ],
        },
        {
            name: 'FACTORY_PROVIDER_WITH_ARGS',
            factory: (value: string) => new MigrationCodeBuilder(value),
            inject: [
                'VALUE_PROVIDER',
            ],
        },
        {
            singleton: true,
            name: 'SINGLETON_CLASS_PROVIDER',
            class: CodeBuilder,
        },
        {
            singleton: true,
            name: 'SINGLETON_FACTORY_PROVIDER',
            factory: () => new CodeBuilder(),
        },
        {
            singleton: true,
            name: 'SINGLETON_CLASS_PROVIDER_WITH_ARGS',
            class: MigrationCodeBuilder,
            inject: [
                'VALUE_PROVIDER',
            ],
        },
        {
            singleton: true,
            name: 'SINGLETON_FACTORY_PROVIDER_WITH_ARGS',
            factory: (value: string) => new MigrationCodeBuilder(value),
            inject: [
                'VALUE_PROVIDER',
            ],
        },
    ];

    const providerWithUndeclaredDependencies: Provider<MigrationCodeBuilder> = {
        name: 'UNDECLARED_DEPENDENCIES',
        class: MigrationCodeBuilder,
        inject: [],
    };

    let container: ServiceContainer;

    beforeAll(() => {
        expect(() => ServiceContainer.init([providerWithUndeclaredDependencies])).toThrow(Error);
        container = ServiceContainer.init(providers);
    });

    test('Positive: Value provider.', () => {
        expect(container.get('VALUE_PROVIDER')).toEqual('Value');
    });

    test('Positive: Class provider.', () => {
        testProvider('CLASS_PROVIDER', CodeBuilder);
    });

    test('Positive: Unnamed class provider.', () => {
        testProvider(CodeBuilder, CodeBuilder);
    });

    test('Positive: Factory provider.', () => {
        testProvider('FACTORY_PROVIDER', CodeBuilder);
    });

    test('Positive: Class provider with arguments.', () => {
        testProvider('CLASS_PROVIDER_WITH_ARGS', MigrationCodeBuilder);
    });

    test('Positive: Unnamed class provider with arguments.', () => {
        testProvider(MigrationCodeBuilder, MigrationCodeBuilder);
    });

    test('Positive: Factory provider with arguments.', () => {
        testProvider('FACTORY_PROVIDER_WITH_ARGS', MigrationCodeBuilder);
    });

    test('Positive: Singleton class provider.', () => {
        testSingletonProvider('SINGLETON_CLASS_PROVIDER', CodeBuilder);
    });

    test('Positive: Singleton factory provider.', () => {
        testSingletonProvider('SINGLETON_FACTORY_PROVIDER', CodeBuilder);
    });

    test('Positive: Singleton class provider with arguments.', () => {
        testSingletonProvider('SINGLETON_CLASS_PROVIDER_WITH_ARGS', MigrationCodeBuilder);
    });

    test('Positive: Singleton factory provider with arguments.', () => {
        testSingletonProvider('SINGLETON_FACTORY_PROVIDER_WITH_ARGS', MigrationCodeBuilder);
    });

    test('Negative: Undeclared name.', () => {
        expect(() => container.get('NON_EXISTED')).toThrow(Error);
    });

    test('Negative: Undeclared class.', () => {
        expect(() => container.get(Function)).toThrow(Error);
    });

    test('Negative: Invalid provider.', () => {
        expect(() => container.get('INVALID_PROVIDER')).toThrow(Error);
    });

    function testProvider<T>(token: Token<any>, instanceType: any): void {
        expect(container.get(token)).toBeInstanceOf(instanceType);
    }

    function testSingletonProvider<T>(name: string, instanceType: new (...args: any[]) => T): void {
        const firstInstance = container.get(name);
        expect(firstInstance).toBeInstanceOf(instanceType);

        const secondInstance = container.get(name);
        expect(secondInstance).toBeInstanceOf(instanceType);

        expect(firstInstance === secondInstance).toEqual(true);
    }
});

import { MigrationCodeBuilder } from '../../../../src/migration/core/builders/migration-code.builder';

describe('Migration code builder functional.', () => {

    test('Positive: Build migration code.', () => {
        const initialTemplate = '["$snapshot", "$upCode", "$downCode"]';

        const code = new MigrationCodeBuilder(initialTemplate)
            .setSnapshot('snapshot')
            .setUpCode('upCode')
            .setDownCode('downCode')
            .build();

        const result = JSON.parse(code);

        expect(result.shift()).toEqual('snapshot');
        expect(result.shift()).toEqual('upCode');
        expect(result.shift()).toEqual('downCode');
    });
});

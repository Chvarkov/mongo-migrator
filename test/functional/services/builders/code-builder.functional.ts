import { CodeBuilder } from '../../../../src/migration/core/builders/code.builder';

describe('Code builder functional.', () => {

    test('Positive: Build code.', () => {
        const code = new CodeBuilder()
            .addTodo('Todo 1')
            .addComment('Comment 1')
            .addExpression('const a = 1;')
            .addComment('Comment 2')
            .addExpression('const b = 2;')
            .addTodo('Todo 2')
            .setCountTabs(2)
            .build();

        const lines = code.split(/\n*\s{4}/g).filter(line => !!line);
        expect(lines).toHaveLength(6);
        expect(lines.shift()).toEqual('// TODO: Todo 1');
        expect(lines.shift()).toEqual('// Comment 1');
        expect(lines.shift()).toEqual('const a = 1;');
        expect(lines.shift()).toEqual('// Comment 2');
        expect(lines.shift()).toEqual('const b = 2;');
        expect(lines.shift()).toEqual('// TODO: Todo 2');
    });
});
